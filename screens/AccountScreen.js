import React from 'react';
import { connect } from 'react-redux';
import * as accountActions from '../actions/accountActions';
import * as productActions from '../actions/productActions';
import { ActivityIndicator, Dimensions, Image, ImageBackground, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Appbar,Divider,List } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import shadow from '../assets/images/shadow.png';
import languages from '../screensTranslations/Account';
import Loading from '../components/Loading';
import Reload from '../components/Reload';
import {GREEN_COLOR} from "@env";

class AccountScreen extends React.Component {
    componentDidMount() {
        this.props.navigation.addListener("didFocus", () => {
            AsyncStorage.setItem('currentTab','Account');
        });
        AsyncStorage.getItem('TOKEN').then((value) => {
            if (value) {
                this.setState({
                    token: value,
                });
            }
        });
        this.setState({
            token: this.props.screenProps.token,
            currentLang: this.props.screenProps.currentLang,
        }, () =>{
            this.getData();
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.logoutReduxRes !== this.props.logoutReduxRes){
            this.props.screenProps.changeLang('');
            AsyncStorage.clear().then(() => {
                this.props.navigation.navigate('AuthLoading');
            });
            this.setState({isLoading: false});
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentLang: '',
            token:'',
            getProfile: '',
            getDataError:'',
            cart:0
        };
        this.getData = this.getData.bind(this);
        this.Logout = this.Logout.bind(this);
        this.NavigateTo = this.NavigateTo.bind(this);
    }
    getData(){
        this.setState({isLoading: false});
        let vars = this.state;
    }
    Logout(){
        this.setState({ isLoading: true}, () => {
            let vars = this.state;
            this.props.logoutRedux(vars);
        });
    };
    NavigateTo(page){
        this.props.navigation.navigate(page);
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else if(this.state.getDataError != ''){
            return (
                <Reload errorMessage={this.state.getDataError} getData={this.getData} />
            );
        }
        else{
            let list;
            if(this.state.token){
                list = [
                    {
                        title: languages[3][this.state.currentLang],
                        icon: 'settings',
                        listNav: 'Profile'
                    },
                    {
                        title: languages[5][this.state.currentLang],
                        icon: 'key',
                        listNav: 'ChangePassword'
                    },
                    {
                        title: languages[6][this.state.currentLang],
                        icon: 'thumbs-up',
                        listNav: 'SignIn'
                    },
                ]
            }
            else{
                list = [
                    {
                        title: languages[1][this.state.currentLang],
                        icon: 'login',
                        listNav: 'SignIn'
                    },
                    {
                        title: languages[2][this.state.currentLang],
                        icon: 'account-plus',
                        listNav: 'SignUp'
                    },
                    {
                        title: languages[4][this.state.currentLang],
                        icon: 'key',
                        listNav: 'ForgotPassword'
                    },
                    {
                        title: languages[7][this.state.currentLang],
                        icon: 'flag',
                        listNav: 'ChangeLanguage'
                    },
                ];
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:GREEN_COLOR}}>
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <View style={{padding:15,flex:1}}>
                        {
                            list.map((item, i) => (
                                <List.Item
                                    key={i}
                                    title={item.title}
                                    left={props => <List.Icon {...props} icon={item.icon} />}
                                    right={props => <List.Icon {...props} icon='chevron-right' />}
                                    onPress={() => this.NavigateTo(item.listNav)}
                                    style={{borderBottomWidth:1,borderBottomColor:'#eee'}}
                                />
                            ))
                        }
                        <List.Item
                            title={languages[8][this.state.currentLang]}
                            left={props => <List.Icon {...props} icon='power' />}
                            right={props => <List.Icon {...props} icon='chevron-right' />}
                            onPress={this.Logout}
                        />
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    scrollWrapper: {
        flex: 1,
        backgroundColor: 'transparent',
    },
    listContainerStyle:{
        backgroundColor: 'transparent',
    },
    listContentContainerStyle:{
        borderBottomWidth:1,
        borderBottomColor:'#ddd',
        paddingVertical:10
    },
});

AccountScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        loading: state.accountReducer.loading,
        logoutReduxRes: state.accountReducer.logoutReduxRes,
    }
}
const mapDispatchToProps = {
    ...productActions,
    ...accountActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
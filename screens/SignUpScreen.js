import React from 'react';
import { connect } from 'react-redux';
import * as accountActions from '../actions/accountActions';
import * as masterActions from '../actions/masterActions';
import { Dimensions, Image, ImageBackground, StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import {Appbar,Button,Icon,TextInput } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Textarea from 'react-native-textarea';
import ToggleSwitch from 'toggle-switch-react-native';
import shadow from '../assets/images/shadow.png';
import Loading from '../components/Loading';
import logo from '../assets/images/logo.png';
import languages from '../screensTranslations/SignUp';
import {GREEN_COLOR,YELLOW_COLOR} from "@env";

class SignUpScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang
        });
    }
    componentDidUpdate(prevprops){
        if(prevprops.signUpRes != this.props.signUpRes){
            if (this.props.signUpRes.token) {
                let legacy_api_key = this.props.signUpRes.payload.legacy_api_key;
                AsyncStorage.setItem('TOKEN', this.props.signUpRes.token).then((value) => {
                    this.setState({finish: true});
                    setTimeout((function() {
                        this.props.navigation.navigate('App');
                    }).bind(this), 5000);
                });
            }
            else{
                if(this.props.signUpRes.status_code == '501' || this.props.signUpRes.status_code == '500'){
                    let errorMsg = [];
                    errorMsg.push(this.props.signUpRes.status_msg);
                    this.setState({errorMessages: errorMsg});
                }
                else{
                    this.setState({errorMessages: this.props.signUpRes.payload});
                }
                setTimeout((function() {
                    this.setState({errorMessages: []});
                }).bind(this), 5000);
                
            }
            this.setState({progressLoading: false});
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            currentLang: '',
            email: '',
            password: '',
            finish: false,
            progressLoading: false,
        };
        this.register = this.register.bind(this);
    }
    register(){
        this.setState({progressLoading: true});
        let vars = this.state;
        this.props.signUp(vars);
    }
    render() {
        if(this.state.finish == true){
            return (
                <View style={{ padding: 5 }}>
                    <Image style={{ marginTop: 60, marginLeft: 20}} source={require('../assets/images/logo.png')} />
                    <View style={{ position: 'absolute', bottom: 120, width: '100%'}}>
                        <Text style={styles.successMessageHeading}>{languages[7][this.state.currentLang]}</Text>
                        <Text style={styles.successMessageText}>{languages[8][this.state.currentLang]}</Text>
                    </View>
                </View>
            );
        }
        else{
            let errorMessagesView = [];
            let errorMessages = this.state.errorMessages;
            if(errorMessages){
                for (let arrName in errorMessages) {
                    errorMessagesView.push(
                        <Text key={arrName} style={styles.errorMessageStyle}>{errorMessages[arrName]}</Text>
                    );
                }
            }
            let createButton;
            if(this.state.progressLoading){
                createButton = (
                    <Button loading disabled={true} mode="contained" color={YELLOW_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={()=>this.register()}>{languages[0][this.props.screenProps.currentLang]}</Button>
                );
            }
            else{
                createButton = (
                    <Button mode="contained" color={YELLOW_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={()=>this.register()}>{languages[0][this.props.screenProps.currentLang]}</Button>
                );
            }
            return (
                <ImageBackground source={require('../assets/images/lang.png')} style={{width: '100%', height: '100%'}}  imageStyle= {{}}>
                    <View style={styles.wrapper}>
                        <Appbar.Header style={{backgroundColor:GREEN_COLOR}} >
                            <Appbar.BackAction style={{marginTop:10}} color="#fff" onPress={() => this.props.navigation.goBack()} />
                            <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                        </Appbar.Header>
                        <View style={{flex:1,padding:10}}>
                            <TextInput
                                keyboardType={'email-address'}
                                label="Email"
                                value={this.state.email}
                                onChangeText={(text) => this.setState({email:text})}
                                mode='outlined'
                                placeholder={languages[2][this.state.currentLang]}
                                left={<TextInput.Icon name="email" />}
                            />
                            <TextInput
                                secureTextEntry={true}
                                label="Password"
                                value={this.state.password}
                                onChangeText={(text) => this.setState({password:text})}
                                mode='outlined'
                                placeholder={languages[3][this.state.currentLang]}
                                left={<TextInput.Icon name="lock" />}
                            />
                            {createButton}
                            {errorMessagesView}
                        </View>
                    </View>
                </ImageBackground>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    errorMessageStyle:{
        backgroundColor:'red',
        padding:5,
        marginVertical:5,
        borderRadius:25,
        alignSelf:'center',
        color:'#fff'
    },
    successMessageHeading:{
        marginBottom:30,
        fontSize:50,
        width: Dimensions.get('window').width * 0.9,
        color:'#FF6040',
        textAlign:'center',
        alignSelf:'center',
        lineHeight:40,
    },
    successMessageText:{
        fontSize:16,
        width: Dimensions.get('window').width * 0.9,
        color:'#FF6040',
        textAlign:'center',
        alignSelf:'center',
        lineHeight:40,
    },
});

SignUpScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        loading: state.accountReducer.loading,
        signUpRes: state.accountReducer.signUpRes,
        getEducationListRes: state.masterReducer.getEducationListRes
    }
}
const mapDispatchToProps = {
    ...accountActions,
    ...masterActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
import React from 'react';
import { Dimensions, Image, StyleSheet, View } from 'react-native';
import {Appbar } from 'react-native-paper';
import Loading from '../components/Loading';
import languages from '../screensTranslations/Preview';
import {GREEN_COLOR} from "@env";

export default class PreviewScreen extends React.Component {
    componentDidMount() {
        this.setState({
            token: this.props.screenProps.token,
            currentShop: this.props.screenProps.currentShop,
            currentLang: this.props.screenProps.currentLang,
            image_link:this.props.navigation.getParam('image_link'),
            type:this.props.navigation.getParam('type')
        }, () =>{
            this.getData();
        });
    }
    componentDidUpdate(prevprops) {
        
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentShop: '',
            currentLang: '',
            token:'',
            image_link: '',
            type: '',
        };
        this.getData = this.getData.bind(this);
    }
    getData(){
        this.setState({isLoading: true});
        if(this.state.image_link != ''){
            this.setState({isLoading: false});
        }
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else{
            let previewImage;
            if(this.state.type == 'blob'){
                previewImage= (
                    <Image source={this.state.image_link} style={styles.previewImage} />
                );
            }
            else{
                previewImage= (
                    <Image source={{uri:this.state.image_link}} style={styles.previewImage} />
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:GREEN_COLOR}} >
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={'Languages'} />
                    </Appbar.Header>
                    <View style={styles.wrapper}>
                        {previewImage}
                    </View>
                </View>
            );
        }
    }
}

PreviewScreen.navigationOptions = {
    header:null
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    previewImage:{
        resizeMode:'contain',
        flex: 1,
        width: Dimensions.get('window').width * 1,
        height: Dimensions.get('window').height * 1,
    }
});
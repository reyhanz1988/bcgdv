import React from 'react';
import { connect } from 'react-redux';
import * as accountActions from '../actions/accountActions';
import { ActivityIndicator, Alert, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {Appbar,Button,Icon,TextInput } from 'react-native-paper';
import shadow from '../assets/images/shadow.png';
import Loading from '../components/Loading';
import logo from '../assets/images/logo.png';
import languages from '../screensTranslations/ForgotPassword';
import {GREEN_COLOR,YELLOW_COLOR} from "@env";

class ForgotPasswordScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentShop: this.props.screenProps.currentShop,
            currentLang: this.props.screenProps.currentLang,
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.forgotPasswordRes !== this.props.forgotPasswordRes){
            if (this.props.forgotPasswordRes.email) {
                this.setState({ success: true });
                setTimeout(() => {
                    this.setState({ 
                        success: false
                    });
                }, 3000)
            } 
        }
    }
    constructor(props) {
        super(props);
    
        this.state = {
            isLoading: false,
            currentShop: '',
            currentLang: '',
            email: '',
            password: '',
            errorMessages: '',
            success: false
        };
        this.ForgotPassword = this.ForgotPassword.bind(this);
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    ForgotPassword() {
        this.setState({
            isLoading: true
        });
        if (this.state.email == '') {
            this.setState({errorMessages: languages[0][this.state.currentLang]});
            setTimeout((function() {
                this.setState({errorMessages: ''});
            }).bind(this), 10000);
        } 
        else {
            if (!this.validateEmail(this.state.email)) {
                this.setState({errorMessages: languages[1][this.state.currentLang]});
                setTimeout((function() {
                    this.setState({errorMessages: ''});
                }).bind(this), 10000);
            } 
            else {
                let vars = this.state;
                this.props.forgotPassword(vars);
            }
        }
        this.setState({
            isLoading: false
        });
    }
    render() {
        let successMessage;
        if(this.state.success){
            successMessage = (
                <Text style={styles.successMessageStyle}>{languages[5][this.state.currentLang]}</Text>
            );
        }
        let errorMessagesView;
        let errorMessages = this.state.errorMessages;
        if(errorMessages){
            errorMessagesView = (
                <Text style={styles.errorMessageStyle}>{errorMessages}</Text>
            );
        }
        let createButton;
        if(this.state.isLoading){
            createButton = (
                <Button loading disabled={true} mode="contained" color={YELLOW_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={this.ForgotPassword}>{languages[4][this.props.screenProps.currentLang]}</Button>
            );
        }
        else{
            createButton = (
                <Button mode="contained" color={YELLOW_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={this.ForgotPassword}>{languages[4][this.props.screenProps.currentLang]}</Button>
            );
        }
        return (
            <View style={styles.wrapper}>
                <Appbar.Header style={{backgroundColor:GREEN_COLOR}} >
                    <Appbar.BackAction style={{marginTop:10}} color="#fff" onPress={() => this.props.navigation.goBack()} />
                    <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[2][this.state.currentLang]} />
                </Appbar.Header>
                <View style={{flex:1,padding:10}}>
                    <TextInput
                        keyboardType={'email-address'}
                        label="Email"
                        value={this.state.email}
                        onChangeText={(text) => this.setState({email:text})}
                        mode='outlined'
                        placeholder={languages[2][this.state.currentLang]}
                        left={<TextInput.Icon name="email" />}
                    />
                    {createButton}
                    {errorMessagesView}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#f7f7f7',
    },
    loginWrapper: {
        backgroundColor: '#ffffff',
        marginHorizontal: 20,
        padding: 20
    },
    containerStyle: {
        marginBottom: 10,
        borderBottomWidth: 0
    },
    inputContainerStyle: {
        marginBottom: 10,
        borderBottomWidth: 0
    },
    inputStyle: {
        marginLeft: 10,
        borderWidth: 1,
        borderColor: '#ededed',
        textAlign: 'center'
    },
    viewShadow: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    shadow: {
        height: 20,
        resizeMode: 'contain',
    },
    buttonWrapper: {
        flexDirection: 'column',
        height: 120,
    },
    buttonView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonStyle: {
        width: 240,
    },
    viewLogo: {
        marginTop: 20,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        resizeMode: 'contain',
        height: 50,
    },
    successMessageStyle:{
        backgroundColor:'#DFF0D8',
        padding:5,
        marginVertical:5,
        borderRadius:25,
        alignSelf:'center',
        color:'#3C959A'
    },
    errorMessageStyle:{
        backgroundColor:'red',
        padding:5,
        marginVertical:5,
        borderRadius:25,
        alignSelf:'center',
        color:'#fff'
    }
});

ForgotPasswordScreen.navigationOptions = {
    header: null
};

function mapStateToProps(state, props) {
    return {
        loading: state.accountReducer.loading,
        forgotPasswordRes: state.accountReducer.forgotPasswordRes
    }
}
const mapDispatchToProps = {
    ...accountActions
};
export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordScreen);
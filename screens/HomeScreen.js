import React from 'react';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import { ActivityIndicator, Dimensions, Image, ImageBackground, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import { Appbar,IconButton,TextInput } from 'react-native-paper';
import TrackPlayer, { usePlaybackState } from "react-native-track-player";
import AsyncStorage from '@react-native-community/async-storage';
import _ from 'lodash';
import Loading from '../components/Loading';
import Player from '../components/Player';
import languages from '../screensTranslations/Home';
import Reload from '../components/Reload';
import {BASE_URL,GREEN_COLOR,YELLOW_COLOR} from "@env";


class HomeScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang
        }, () =>{
            this.getData();
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.screenProps.currentLang != this.props.screenProps.currentLang){
            this.setState({
                currentLang: this.props.screenProps.currentLang
            }, () =>{
                this.getData();
            });
        }
        if(prevprops.itunesSearchRes != this.props.itunesSearchRes){
            console.log(this.props.itunesSearchRes);
            this.setState({ 
                itunesSearch: this.props.itunesSearchRes,
            }, () =>{
                this.setState({
                    getDataError: '',
                    isLoading: false
                });
            });
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentLang: '',
            search:'Taylor Swift',
            itunesSearch: [],
            currentPlay: '',
        };
        this.getData = this.getData.bind(this);
        this.togglePlayback = this.togglePlayback.bind(this);
        this.play = this.play.bind(this);
        this.autoPlay = this.autoPlay.bind(this);
        this.skipToNext = this.skipToNext.bind(this);
        this.skipToPrevious = this.skipToPrevious.bind(this);
    }
    getData(){
        let vars = this.state;
        this.props.itunesSearch(vars);
        TrackPlayer.setupPlayer();
        TrackPlayer.updateOptions({
            stopWithApp: true,
            capabilities: [
                TrackPlayer.CAPABILITY_PLAY,
                TrackPlayer.CAPABILITY_PLAY_FROM_ID,
                TrackPlayer.CAPABILITY_PAUSE,
                TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
                TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
                TrackPlayer.CAPABILITY_STOP
            ],
            compactCapabilities: [
                TrackPlayer.CAPABILITY_PLAY,
                TrackPlayer.CAPABILITY_PLAY_FROM_ID,
                TrackPlayer.CAPABILITY_PAUSE,
                TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
                TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
                TrackPlayer.CAPABILITY_STOP
            ]
        });
    }
    togglePlayback(){
        TrackPlayer.stop();
    }
    play(currentPlay){
        this.setState({
            currentPlay: currentPlay
        }, () =>{
            let data = this.state.itunesSearch.results;
            let playlists = [];
            for(let i=0;i<data.length;i++){
                playlists.push({
                    id: data[i].trackId,
                    url: data[i].previewUrl,
                    title: data[i].trackName,
                    artist: data[i].artistName,
                    album: data[i].collectionName,
                    genre: data[i].primaryGenreName,
                    date: data[i].releaseDate,
                    artwork: data[i].artworkUrl60,
                    duration: Math.floor(data[i].trackTimeMillis/1000)
                });
            }
            TrackPlayer.reset();
            TrackPlayer.add(playlists);
            TrackPlayer.skip(currentPlay.toString());
            TrackPlayer.play();
        });
    }
    autoPlay(currentPlay){
        this.setState({
            currentPlay: currentPlay
        });
    }
    skipToNext() {
        try {
            TrackPlayer.skipToNext();
        } 
        catch (_) {}
    }
    skipToPrevious() {
        try {
            TrackPlayer.skipToPrevious();
        } 
        catch (_) {}
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else{
            let data = this.state.itunesSearch.results;
            let listView = [];
            if(data.length > 0){
                for(let i=0;i<data.length;i++){
                    let trackIcon;
                    if(this.state.currentPlay == data[i].trackId){
                        trackIcon = (
                            <IconButton
                                icon="music"
                                color={YELLOW_COLOR}
                                size={30}
                            />
                        );
                    }
                    else{
                        trackIcon = (
                            <IconButton
                                icon="youtube"
                                color={GREEN_COLOR}
                                size={30}
                                onPress={() => this.play(data[i].trackId)}
                            />
                        );
                    }
                    listView.push(
                        <View key={'list'+i} style={{flex:1,flexDirection:'row',paddingVertical:10,borderBottomWidth:1,borderBottomColor:"#eee",marginLeft:10,marginRight:10}}>
                            <View style={{width:70,justifyContent:'center',alignItems:'center'}}>
                                <Image style={{width:60,height:60}} source={{uri:data[i].artworkUrl60}} />
                            </View>
                            <View style={{flex:1}}>
                                <Text style={{color:'#333',fontSize:16,fontWeight:'bold'}}>{data[i].trackName}</Text>
                                <Text style={{color:'#555',fontSize:14}}>{data[i].artistName}</Text>
                                <Text style={{color:'#777',fontSize:12}}>{data[i].collectionName}</Text>
                            </View>
                            <View style={{width:40,justifyContent:'center',alignItems:'center'}}>
                                {trackIcon}
                            </View>
                        </View>
                    )
                }
            }
            let player;
            if(this.state.currentPlay !== ''){
                player = (
                    <Player
                        currentPlay={this.state.currentPlay}
                        autoPlay={this.autoPlay}
                    />
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:GREEN_COLOR}}>
                        <TextInput
                            style={{height:30,flex:1,backgroundColor:'#fff'}}
                            mode="outlined"
                            placeholder={languages[0][this.props.screenProps.currentLang]}
                            value={this.state.search}
                            onChangeText={(text) => this.setState({search: text})}
                            right={<TextInput.Icon style={{marginTop:15}} name="magnify" onPress={()=>this.getData()} />}
                            selectionColor={GREEN_COLOR}
                        />
                    </Appbar.Header>
                    <ScrollView style={{flex:1}}>
                        {listView}
                        
                    </ScrollView>
                    <View style={{width:Dimensions.get('window').width}}>
                        {player}
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper:{
        flex:1,
    },
    mainView:{
        flex:1,
        flexDirection: 'row',
        marginTop:10
    },
});

HomeScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        loading: state.productReducer.loading,
        itunesSearchRes: state.productReducer.itunesSearchRes,
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
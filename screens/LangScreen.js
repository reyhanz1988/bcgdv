import React from 'react';
import { connect } from 'react-redux';
import * as masterActions from '../actions/masterActions';
import { ActivityIndicator, Dimensions, Image, ImageBackground, Picker, ScrollView, StyleSheet, Text, View } from 'react-native';
import {Appbar,Button} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import RNPickerSelect from 'react-native-picker-select';
import shadow from '../assets/images/shadow.png';
import Loading from '../components/Loading';
import logo from '../assets/images/logo.png';
import RNRestart from 'react-native-restart';
import Reload from '../components/Reload';
import {GREEN_COLOR} from "@env";

class LangScreen extends React.Component {
    componentDidMount() {
        this.getData();
    }
    componentDidUpdate(prevprops) {
        if(prevprops.getLangListRes != this.props.getLangListRes){
            if(this.props.getLangListRes == 'error'){
                this.setState({
                    getDataError: this.props.getLangListRes,
                    isLoading: false
                });
            }
            else{
                if(this.props.getLangListRes.status_msg){
                    this.setState({
                        getDataError: this.props.getLangListRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    this.setState({ 
                        langList: this.props.getLangListRes ,
                        isLoading:false
                    });
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            lang:'id',
            langList:[],
            getDataError:''
        };
        this.getData = this.getData.bind(this);
        this.confirmSelection = this.confirmSelection.bind(this);
    }
    getData(){
        this.props.getLangList();
    }
    confirmSelection(){
        AsyncStorage.setItem('LANG', this.state.lang).then((b) => {
            this.props.screenProps.changeLang(this.state.lang);
            RNRestart.Restart();
        });
    }
    render() {
        if(this.state.isLoading) {
            return (
                <Loading />
            );
        }
        else if(this.state.getDataError != ''){
            return (
                <Reload errorMessage={this.state.getDataError} getData={this.getData} />
            );
        }
        else{
            let langList = this.state.langList;
            let langItems = [];
            for(let i=0;i<langList.length;i++){
                langItems.push(
                    {key:i, label:langList[i].language, value:langList[i].language_iso}
                );
            }
            return (
                <ImageBackground source={require('../assets/images/lang.png')} style={{width: '100%', height: '100%'}}  imageStyle= {{}}>
                    <View style={styles.wrapper}>
                        <Appbar.Header style={{backgroundColor:GREEN_COLOR}} >
                            <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title='Languages' />
                        </Appbar.Header>
                        <ScrollView>
                            <View style={styles.profileWrapper}>
                                <View style={{flex:1,marginTop:10}}>
                                    <Text style={{paddingLeft:8,color:'#4bb9e6'}}>Select Language</Text>
                                    <RNPickerSelect
                                        value={this.state.lang}
                                        onValueChange={(itemValue, itemIndex) =>
                                            this.setState({lang: itemValue})
                                        }
                                        items={langItems}
                                        style={pickerSelectStyles}
                                    />
                                </View>
                                <View style={{flex:1,alignItems:'center'}}>
                                    <Button mode="contained" color={GREEN_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={this.confirmSelection}>Proceed</Button>
                                </View>
                            </View>
                            <View style={styles.viewShadow}>
                                <Image source={shadow} style={styles.shadow}/>
                            </View>
                        </ScrollView>
                    </View>
                </ImageBackground>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    profileWrapper:{
        marginHorizontal:20,
        backgroundColor:'#ffffff',
        flex:1,
        padding:20
    },
    viewShadow: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    shadow: {
        height: 20,
        resizeMode: 'contain',
        width: Dimensions.get('window').width * 0.9,
    },
    dropdownStyle:{

    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: 16,
      paddingVertical: 12,
      paddingHorizontal: 10,
      color: '#000000',
      paddingRight: 30, // to ensure the text is never behind the icon
    }
});

LangScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        loading: state.masterReducer.loading,
        getLangListRes: state.masterReducer.getLangListRes
    }
}
const mapDispatchToProps = {
    ...masterActions
};
export default connect(mapStateToProps, mapDispatchToProps)(LangScreen);
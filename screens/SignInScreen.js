import React from 'react';
import { connect } from 'react-redux';
import * as accountActions from '../actions/accountActions';
import * as masterActions from '../actions/masterActions';
import { Dimensions, Image, ImageBackground, StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import {Appbar,Button,Icon,TextInput } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import base64 from 'react-native-base64'
import shadow from '../assets/images/shadow.png';
import Loading from '../components/Loading';
import logo from '../assets/images/logo.png';
import languages from '../screensTranslations/SignIn';
import {GREEN_COLOR,YELLOW_COLOR} from "@env";

class SignInScreen extends React.Component {
    async componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang,
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.checkAccountLoginRes != this.props.checkAccountLoginRes){
            if (this.props.checkAccountLoginRes.token) {
                AsyncStorage.setItem('TOKEN', this.props.checkAccountLoginRes.token).then((value) => {
                    this.props.navigation.navigate('App');
                });
            } 
            else{
                if(this.props.checkAccountLoginRes.status_code == '501' || this.props.checkAccountLoginRes.status_code == '500'){
                    let errorMsg = [];
                    errorMsg.push(this.props.checkAccountLoginRes.status_msg);
                    this.setState({errorMessages: errorMsg});
                }
                else{
                    this.setState({errorMessages: {0:this.props.checkAccountLoginRes.error}});
                }
                setTimeout((function() {
                    this.setState({errorMessages: []});
                }).bind(this), 5000);
                
            }
            this.setState({progressLoading: false});
        }
    }
    constructor(props) {
        super(props);
    
        this.state = {
            progressLoading: false,
            currentLang: '',
            email: '',
            password: '',
            errorMessages: '',
        };
        this.validateEmail = this.validateEmail.bind(this);
        this.SignIn = this.SignIn.bind(this);
    }
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    SignIn(){
        if (this.state.email == '' || this.state.password == '') {
            this.setState({errorMessages: {0:languages[2][this.state.currentLang]}});
            setTimeout((function() {
                this.setState({
                    errorMessages: ''
                });
            }).bind(this), 3000);
        } 
        else {
            if (!this.validateEmail(this.state.email)) {
                this.setState({errorMessages: {0:languages[3][this.state.currentLang]}});
                setTimeout((function() {
                    this.setState({
                        errorMessages: ''
                    });
                }).bind(this), 3000);
            } else {
                this.setState({progressLoading: true});
                let vars = this.state;
                this.props.checkAccountLogin(vars);
            }
        }
    };
    render() {
        let errorMessagesView = [];
        let errorMessages = this.state.errorMessages;
        if(errorMessages){
            for (let arrName in errorMessages) {
                errorMessagesView.push(
                    <Text key={arrName} style={styles.errorMessageStyle}>{errorMessages[arrName]}</Text>
                );
            }
        }
        let signInButton;
        if(this.state.progressLoading){
            signInButton = (
                <Button loading disabled={true} mode="contained" color={YELLOW_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={()=>this.SignIn()}>{languages[0][this.props.screenProps.currentLang]}</Button>
            );
        }
        else{
            signInButton = (
                <Button mode="contained" color={YELLOW_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={()=>this.SignIn()}>{languages[0][this.props.screenProps.currentLang]}</Button>
            );
        }
        return (
            <ImageBackground source={require('../assets/images/lang.png')} style={{width: '100%', height: '100%'}}  imageStyle= {{}}>
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:GREEN_COLOR}} >
                        <Appbar.BackAction style={{marginTop:10}} color="#fff" onPress={() => this.props.navigation.goBack()} />
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <View style={{flex:1,padding:10}}>
                        <TextInput
                            keyboardType={'email-address'}
                            label="Email"
                            value={this.state.email}
                            onChangeText={(text) => this.setState({email:text})}
                            mode='outlined'
                            placeholder={languages[2][this.state.currentLang]}
                            left={<TextInput.Icon name="email" />}
                        />
                        <TextInput
                            secureTextEntry={true}
                            label="Password"
                            value={this.state.password}
                            onChangeText={(text) => this.setState({password:text})}
                            mode='outlined'
                            placeholder={languages[3][this.state.currentLang]}
                            left={<TextInput.Icon name="lock" />}
                        />
                        {signInButton}
                        {errorMessagesView}
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    errorMessageStyle:{
        backgroundColor:'red',
        padding:5,
        marginVertical:5,
        borderRadius:25,
        alignSelf:'center',
        color:'#fff'
    },
});

SignInScreen.navigationOptions = {
    header: null
};

function mapStateToProps(state, props) {
    return {
        loading: state.accountReducer.loading,
        checkAccountLoginRes: state.accountReducer.checkAccountLoginRes,
    }
}
const mapDispatchToProps = {
    ...accountActions,
    ...masterActions
};
export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);
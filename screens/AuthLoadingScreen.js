import React from 'react';
import {ActivityIndicator, StatusBar, Platform, StyleSheet, View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class AuthLoadingScreen extends React.Component {
    componentDidMount() {
        if(Platform.OS !== 'ios'){
            StatusBar.setBackgroundColor('#ffffff');
        }
        StatusBar.setBarStyle('dark-content');
        AsyncStorage.getItem('LANG').then((value) => {
            if (value) {
                this.setState({
                    currentLang: value
                }, () =>{
                    this.props.navigation.navigate('App');
                });
            } 
            else {
                this.props.navigation.navigate('Lang');
            }
        });
    }
    constructor(props) {
        super(props);
        this.state = {
            currentLang: '',
            token:'',
        };
    }
    render() {
        return (
            <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
                <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
                <ActivityIndicator />
            </View>
        );
    }
}
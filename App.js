import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import AppNavigator from './navigation/AppNavigator';
import NavigationService from './navigation/NavigationService';
import { Provider } from 'react-redux';
import store from './store';
import Orientation from 'react-native-orientation-locker';

console.disableYellowBox = true; 

export default class App extends React.Component {
    componentDidMount() {
        Orientation.getAutoRotateState((rotationLock) => this.setState({rotationLock}));
        Orientation.lockToPortrait();
        AsyncStorage.getItem('LANG').then((value) => {
            this.setState({ currentLang: value });
        });
        AsyncStorage.getItem('TOKEN').then((value) => {
            if (value) {
                this.setState({
                    token: value,
                });
            }
            else{
                this.setState({
                    token: '',
                });
            }
        });
    }
    componentDidUpdate(prevprops) {
        
    }
    constructor(props) {
        super(props);
        this.state = {
            rotationLock: '',
            token: '',
            currentLang: null,
            isLoading: true,
        };
        this.changeLang = this.changeLang.bind(this);
    }
    componentWillUnmount() {

    }
    changeLang(lang){
        this.setState({ currentLang: lang });
    }
    render() {
        let theApp;
        theApp = (
            <View style={styles.container}>
                {Platform.OS === 'ios' && <StatusBar />}
                <AppNavigator
                    screenProps={{  changeLang:this.changeLang,
                                    token:this.state.token,
                                    currentLang:this.state.currentLang}}
                    ref={
                        navigatorRef => {
                            NavigationService.setTopLevelNavigator(navigatorRef);
                        }} 
                />
            </View>
        );
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    {theApp}
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    }
});

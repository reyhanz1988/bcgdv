import ProductApi from '../api/ProductApi';
import { createAsyncActionType, errorToastMessage } from '../components/util';
import NavigationService from '../navigation/NavigationService';

// itunesSearch
export const ITUNES_SEARCH = createAsyncActionType('ITUNES_SEARCH');
export const itunesSearch = vars => dispatch => {
    dispatch({
        type: ITUNES_SEARCH.REQUESTED,
    });
    ProductApi.itunesSearch(vars)
    .then(response => {
        dispatch({
            type: ITUNES_SEARCH.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: ITUNES_SEARCH.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};
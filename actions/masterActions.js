import MasterApi from '../api/MasterApi';
import { createAsyncActionType, errorToastMessage } from '../components/util';
import NavigationService from '../navigation/NavigationService';

// getLangList
export const GET_LANG_LIST = createAsyncActionType('GET_LANG_LIST');
export const getLangList = vars => dispatch => {
    dispatch({
        type: GET_LANG_LIST.REQUESTED,
    });
    MasterApi.getLangList()
    .then(response => {
        dispatch({
            type: GET_LANG_LIST.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_LANG_LIST.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// postLang
export const POST_LANG = createAsyncActionType('POST_LANG');
export const postLang = vars => dispatch => {
    dispatch({
        type: POST_LANG.REQUESTED,
        payload: { vars },
    });  
};

// getEducationList
export const GET_EDUCATION_LIST = createAsyncActionType('GET_EDUCATION_LIST');
export const getEducationList = vars => dispatch => {
    dispatch({
        type: GET_EDUCATION_LIST.REQUESTED,
    });
    MasterApi.getEducationList(vars)
    .then(response => {
        dispatch({
            type: GET_EDUCATION_LIST.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_EDUCATION_LIST.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};
import AccountApi from '../api/AccountApi';
import { createAsyncActionType, errorToastMessage } from '../components/util';
import NavigationService from '../navigation/NavigationService';

// checkAccountLogin
export const CHECK_ACCOUNT_LOGIN = createAsyncActionType('CHECK_ACCOUNT_LOGIN');
export const checkAccountLogin = vars => dispatch => {
    dispatch({
        type: CHECK_ACCOUNT_LOGIN.REQUESTED,
    });
    AccountApi.checkAccountLogin(vars)
    .then(response => {
        dispatch({
            type: CHECK_ACCOUNT_LOGIN.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: CHECK_ACCOUNT_LOGIN.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// signUp
export const SIGN_UP = createAsyncActionType('SIGN_UP');
export const signUp = vars => dispatch => {
    dispatch({
        type: SIGN_UP.REQUESTED,
    });
    AccountApi.signUp(vars)
    .then(response => {
        dispatch({
            type: SIGN_UP.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: SIGN_UP.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// logoutRedux
export const LOGOUT_REDUX = createAsyncActionType('LOGOUT_REDUX');
export const logoutRedux = vars => dispatch => {
    dispatch({
        type: LOGOUT_REDUX.REQUESTED,
    });
    AccountApi.logoutRedux(vars)
    .then(response => {
        dispatch({
            type: LOGOUT_REDUX.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: LOGOUT_REDUX.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../navigation/NavigationService';
import {BASE_URL} from "@env";

let AccountApi = {
    checkAccountLogin(vars) {
        let params = {
            method: 'POST',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: vars.email,
                password: vars.password,
            })
        };
        return fetch(BASE_URL + '/api/login', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("BCGDV checkAccountLogin => " + error);
            });
    },
    signUp(vars) {
        let params = {
            method: 'POST',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: vars.email,
                password: vars.password,
            })
        };
        return fetch(BASE_URL + '/api/signup', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("BCGDV signUp => " + error);
            });
    },
    logoutRedux(vars) {
        let params = {
            method: 'DELETE',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+vars.token
            }
        };
        return fetch(BASE_URL + '/auth/login/'+vars.currentShop, params)
            .then((res) => vars)
            .catch((error) => {
                return 'error';
                console.warn("BCGDV logoutRedux => " + error);
            });
    },
};

module.exports = AccountApi;
import {BASE_URL,API_ITUNES} from "@env"

let ProductApi = {
    itunesSearch(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        let str = vars.search;
        let replaced = str.split(' ').join('+');
        return fetch(API_ITUNES + '/search?media=music&term='+replaced, params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("BCGDV itunesSearch => " + error);
            });
    },
};

module.exports = ProductApi;

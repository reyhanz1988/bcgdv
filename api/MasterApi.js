import {BASE_URL} from "@env"

let MasterApi = {
    getLangList() {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': 'en-US',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(BASE_URL + '/api/languages', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("BCGDV getLangList => " + error);
            });
    }
};

module.exports = MasterApi;
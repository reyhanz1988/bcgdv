import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { IconButton } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import HomeScreen from '../screens/HomeScreen';
import AccountScreen from '../screens/AccountScreen';
import PreviewScreen from '../screens/PreviewScreen';

import SignInScreen from '../screens/SignInScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import SignUpScreen from '../screens/SignUpScreen';

import languages from '../screensTranslations/AppStack';
import {GREEN_COLOR,YELLOW_COLOR} from "@env";

let HomeStack = createStackNavigator({
    Home: HomeScreen,
    Preview: PreviewScreen
});
let AccountStack = createStackNavigator({
    Account: AccountScreen,
    SignIn: SignInScreen,
    SignUp: SignUpScreen,
    ForgotPassword: ForgotPasswordScreen,
    Preview: PreviewScreen
});
const styles = StyleSheet.create({
    icons: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
    },
});

async function getLang() {
    const currentLang = await AsyncStorage.getItem('LANG');
    HomeStack.navigationOptions = ({ navigation }) => {
        return {
            tabBarLabel: languages[0][currentLang],
            tabBarIcon: ({ focused }) => (
                focused ? <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='home' color={YELLOW_COLOR} /> : <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='home' color='#ffffff' />
            ),
            tabBarOptions: {
                showIcon: true,
                upperCaseLabel: false,
                indicatorStyle:{borderBottomWidth:0,backgroundColor: 'transparent',height:0},
                tabStyle: {backgroundColor: GREEN_COLOR,height:70},
                activeTintColor: '#ffffff',
                inactiveTintColor: '#ffffff',
                labelStyle :{fontSize:12,width:76,marginTop:2,}
            }
        }
    };
    AccountStack.navigationOptions = ({ navigation }) => {
        return {
            tabBarLabel: languages[1][currentLang],
            tabBarIcon: ({ focused }) => (
                focused ? <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='account' color={YELLOW_COLOR} /> : <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='account' color='#ffffff' />
            ),
            tabBarOptions: {
                showIcon: true,
                upperCaseLabel: false,
                indicatorStyle:{borderBottomWidth:0,backgroundColor: 'transparent',height:0},
                tabStyle: {backgroundColor: GREEN_COLOR,height:70},
                activeTintColor: '#ffffff',
                inactiveTintColor: '#ffffff',
                labelStyle :{fontSize:12,width:76,marginTop:2,}
            }
        }
    };
}
getLang();

const AppStack = createMaterialTopTabNavigator(
{
    HomeStack,
    AccountStack,
}, 
{
    swipeEnabled: false,
    animationEnabled: true,
    tabBarPosition:'bottom',
});

export {AppStack}
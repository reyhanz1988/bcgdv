import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import HomeScreen from '../screens/HomeScreen';
import LangScreen from '../screens/LangScreen';
import {AppStack} from '../navigation/AppStack';

const LangStack = createStackNavigator({
    Lang: LangScreen
});

export default createAppContainer(
    createSwitchNavigator({
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Lang: LangStack
    },
    {
        initialRouteName: 'AuthLoading',
    })
);
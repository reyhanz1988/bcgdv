module.exports =  [
/*0*/
{
    en: "Email cannot be empty",
    id: "Email tidak boleh kosong",
},

/*1*/
{
    en: "Email address is incorrect",
    id: "Email salah",

},

/*2*/
{
    en: "Forgot password",
    id: "Lupa password",

},

/*3*/
{
    en: "Email",
    id: "Email",

},

/*4*/
{
    en: "Send Me Instructions",
    id: "Kirimi Saya Instruksi",

},

/*5*/
{
    en: "A confirmation e-mail has been sent to your email address",
    id: "Email konfirmasi telah dikirim ke alamat email Anda",

},
];
module.exports =  [
/*0*/
{
    en: "Account",
    id: "Akun",

},

/*1*/
{
    en: "SignIn",
    id: "Masuk",
    
},

/*2*/
{
    en: "SignUp",
    id: "Daftar",

},

/*3*/
{
    en: "Profile",
    id: "Profil",

},

/*4*/
{
    en: "Forgot Password",
    id: "Lupa Password",

},

/*5*/
{
    en: "Change Password",
    id: "Ganti Password",

},

/*6*/
{
    en: "Subscription",
    id: "Berlangganan",

},

/*7*/
{
    en: "Change Language",
    id: "Ganti Bahasa",

},

/*8*/
{
    en: "SignOut",
    id: "Keluar",

},

/*9*/
{
    en: "Change Language",
    id: "Ganti Bahasa",

},

/*10*/
{
    en: "Logout",
    id: "Keluar",

},

];
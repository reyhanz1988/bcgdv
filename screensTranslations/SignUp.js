module.exports =  [
/*0*/
{
    en: "SignUp",
    id: "Daftar",

},

/*1*/
{
    en: "Name *",
    id: "Nama *",

},

/*2*/
{
    en: "Email *",
    id: "Email *",
},

/*3*/
{
    en: "Password *",
    id: "Password *",
    
},

/*4*/
{
    en: "Date of Birth",
    id: "Tanggal Lahir",

},

/*5*/
{
    en: "Address *",
    id: "Alamat",

},

/*6*/
{
    en: "Phone *",
    id: "Telepon *",
    
},

/*7*/
{
    de: "Vielen Dank für die Registrierung.",
    en: "Thanks for registering.",
    es: "Gracias por registrarse",
    fr: "Merci de vous inscrire.",
    it: "Grazie per esserti registrato/a",
    nl: "Thanks for registering.",
    no: "Thanks for registering.",
    pt: "",

},

/*8*/
{
    de: "Du wirst in wenigen Sekunden auf dein Konto umgeleitet...bitte warten",
    en: "You will be redirected to your account in a few seconds..please wait",
    es: "Será redireccionado a su cuenta en algunos segundos.... por favor espere",
    fr: "Vous serez redirigé vers votre compte dans quelques secondes...veuillez patienter",
    it: "Sarai reindirizzato al tuo profilo in qualche secondo. Attendere prego.",
    nl: "You will be redirected to your account in a few seconds..please wait",
    no: "You will be redirected to your account in a few seconds..please wait",
    pt: "",

},
];
module.exports =  [
/*0*/
{
    de: "Wir haben Probleme, die Daten zu erhalten. Drück auf den Knopf oben, um sie neu zu laden. Vielen Dank.",
    en: "We are having problem to get the data, press the button above to reload it. Thank you",
    es: "We are having problem to get the data, press the button above to reload it. Thank you",
    fr: "Nous avons des problèmes pour obtenir les données, appuyez sur le bouton ci-dessus pour les recharger. Merci",
    it: "We are having problem to get the data, press the button above to reload it. Thank you",
    nl: "We are having problem to get the data, press the button above to reload it. Thank you",
    no: "We are having problem to get the data, press the button above to reload it. Thank you",
    pt: "",

},

/*1*/
{
    de: "Neu laden",
    en: "Reload",
    es: "Reload",
    fr: "Recharger",
    it: "Reload",
    nl: "Reload",
    no: "Reload",
    pt: "",

},

/*2*/
{
    de: "Stornieren",
    en: "Cancel",
    es: "Cancelar",
    fr: "Annuler",
    it: "Annulla",
    nl: "Annuleren",
    no: "Avbryt",
    pt: "",

},
];
module.exports =  [
/*0*/
{
    en: "SignIn",
    id: "Masuk",

},

/*1*/
{
    en: "SignIn Failed",
    id: "Gagal Masuk",

},

/*2*/
{
    en: "Email or password cannot be empty",
    id: "Email atau password tidak boleh kosong",

},

/*3*/
{
    en: "Email format is incorrect",
    id: "Format email salah",
},

/*4*/
{
    en: "Wrong email or password",
    id: "Email atau password salah",

},

/*5*/
{
    en: "Forgot password",
    id: "Lupa password",

},

/*6*/
{
    en: "Email",
    id: "Email",

},

/*7*/
{
    en: "Password",
    id: "Password",

},

/*8*/
{
    en: "Create Account",
    id: "Buat Akun",

},

/*9*/
{
    en: "Loading",
    id: "Loading",

},

/*10*/
{
    en: "Something went wrong, please try again.",
    id: "Ada yang salah, silakan coba lagi.",

},

/*11*/
{
    en: "Relax! Please do it again after one minute.",
    id: "Bersantai! Silakan lakukan lagi setelah satu menit.",

},
];
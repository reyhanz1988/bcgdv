import {GET_LANG_LIST, POST_LANG, GET_EDUCATION_LIST} from "../actions/masterActions";

const initialState = {
    successMsg: '',
    errorMsg: '',
    loading: true,
    getLangListRes:[],
    postLangRes:[],
    getEducationListRes:[]
};

const masterReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_LANG_LIST                                                                                                                           */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_LANG_LIST.REQUESTED:
            return { ...state, loading: true };
        case GET_LANG_LIST.SUCCESS:
            return {
                ...state,
                getLangListRes: payload.response,
                loading: false,
                errorMsg: '',
            };
        case GET_LANG_LIST.ERROR:
            return { ...state, loading: false, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  POST_LANG_LIST                                                                                                                          */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case POST_LANG.SUCCESS:
            return {
                ...state,
                postLangRes: payload,
            };
        
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_EDUCATION_LIST                                                                                                                           */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_EDUCATION_LIST.REQUESTED:
            return { ...state, loading: true };
        case GET_EDUCATION_LIST.SUCCESS:
            return {
                ...state,
                getEducationListRes: payload.response,
                loading: false,
                errorMsg: '',
            };
        case GET_EDUCATION_LIST.ERROR:
            return { ...state, loading: false, errorMsg: payload.error };

        default:
            return state
    }
}

function cloneObject(object){
    return JSON.parse(JSON.stringify(object));
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}


export default masterReducer
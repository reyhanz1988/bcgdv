import {
    ITUNES_SEARCH
} from "../actions/productActions";

const initialState = {
    successMsg: '',
    errorMsg: '',
    loading: true,
    itunesSearchRes:[]
};

const productReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  ITUNES_SEARCH                                                                                                                          */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case ITUNES_SEARCH.REQUESTED:
            return { ...state, loading: true };
        case ITUNES_SEARCH.SUCCESS:
            return {
                ...state,
                itunesSearchRes: payload.response,
                loading: false,
                errorMsg: '',
            };
        case ITUNES_SEARCH.ERROR:
            return { ...state, loading: false, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        default:
            return state
    }
}

function cloneObject(object){
    return JSON.parse(JSON.stringify(object));
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}


export default productReducer
import {CHECK_ACCOUNT_LOGIN, SIGN_UP, LOGOUT_REDUX} from "../actions/accountActions";

const initialState = {
    successMsg: '',
    errorMsg: '',
    loading: true,
    checkAccountLoginRes:[],
    signUpRes:[],
    logoutReduxRes:[]
};

const accountReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  CHECK_ACCOUNT_LOGIN                                                                                                                     */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case CHECK_ACCOUNT_LOGIN.REQUESTED:
            return { ...state, loading: true };
        case CHECK_ACCOUNT_LOGIN.SUCCESS:
            return {
                ...state,
                checkAccountLoginRes: payload.response,
                loading: false,
                errorMsg: '',
            };
        case CHECK_ACCOUNT_LOGIN.ERROR:
            return { ...state, loading: false, errorMsg: payload.error };
        
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  SIGN_UP                                                                                                                          */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case SIGN_UP.REQUESTED:
            return { ...state, loading: true };
        case SIGN_UP.SUCCESS:
            return {
                ...state,
                signUpRes: payload.response,
                loading: false,
                errorMsg: '',
            };
        case SIGN_UP.ERROR:
            return { ...state, loading: false, errorMsg: payload.error };
              
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  LOGOUT                                                                                                                                  */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case LOGOUT_REDUX.REQUESTED:
            return { ...state, loading: true };
        case LOGOUT_REDUX.SUCCESS:
            return {
                ...state,
                logoutReduxRes: payload.response,
                loading: false,
                errorMsg: '',
            };
        case LOGOUT_REDUX.ERROR:
            return { ...state, loading: false, errorMsg: payload.error };
        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        default:
            return state
    }
}

function cloneObject(object){
    return JSON.parse(JSON.stringify(object));
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}


export default accountReducer
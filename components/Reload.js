import React, {Component} from 'react';
import { ActivityIndicator, Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import {Button} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import logo from '../assets/images/logo.png';
import languages from '../screensTranslations/Reload';
import {GREEN_COLOR,YELLOW_COLOR} from "@env";

class Reload extends Component {
    componentDidMount() {
        AsyncStorage.getItem('LANG').then((value) => {
            if(value){
                this.setState({ currentLang: value });
            }
        });
    }
    constructor() {
        super()
        this.state = {
            currentLang: 'en',
        }
    }
    render() {
        let buttonSet;
        if(this.props.cancel){
            buttonSet = (
                <View style={styles.buttonWrapper}>
                    <View style={styles.buttonView}>
                        <Button mode="contained" color={GREEN_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={()=>this.props.getData()}>{languages[1][this.props.screenProps.currentLang]}</Button>
                    </View>

                    <View style={styles.buttonView}>
                        <Button mode="outlined" color={GREEN_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={()=>this.props.cancel()}>{languages[2][this.props.screenProps.currentLang]}</Button>
                    </View>
                </View>
            );
        }else{
            buttonSet = (
                <Button mode="contained" color={GREEN_COLOR} uppercase={false} style={{marginTop:20,padding:5}} labelStyle={{color:'#ffffff'}} onPress={()=>this.props.getData()}>{languages[1][this.props.screenProps.currentLang]}</Button>
            );
        }
        return (
            <View style={styles.loadingWrapper}>
                <Image source={logo} style={styles.bgLoading} />
                <Text style={styles.textStyle}>{this.props.errorMessage}</Text>
                <Text style={styles.textStyle}>{languages[0][this.state.currentLang]}</Text>
                {buttonSet}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    loadingWrapper: {
        alignSelf:'center',
        justifyContent:'center',
        width:'80%',
        height:'100%'
    },
    bgLoading:{
        position: 'absolute',
        bottom: 20,
        right: 20,
        width: Dimensions.get('window').width * 0.4,
        resizeMode:'contain'
    },
    textStyle:{
        alignSelf:'center',
        textAlign:'center',
        color:'#000000',
        marginTop:20
    },
    buttonWrapper: {
        marginTop: 40,
        flexDirection: 'row',
        height: 60,
    },
    buttonView: {
        flex: 1,
        alignItems: 'center',
    },
    buttonStyle: {
        width: '100%',
    },
})
export default Reload
import React, { useState } from "react";
import PropTypes from "prop-types";
import TrackPlayer, {
  useTrackPlayerProgress,
  usePlaybackState,
  useTrackPlayerEvents
} from "react-native-track-player";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewPropTypes
} from "react-native";
import { IconButton } from 'react-native-paper';
import {BASE_URL,GREEN_COLOR,YELLOW_COLOR} from "@env";

function ProgressBar() {
  const progress = useTrackPlayerProgress();

  return (
    <View style={styles.progress}>
      <View style={{ flex: progress.position, backgroundColor: "white" }} />
      <View
        style={{
          flex: progress.duration - progress.position,
          backgroundColor: "grey"
        }}
      />
    </View>
  );
}

export default function Player(props) {
  const playbackState = usePlaybackState();
  useTrackPlayerEvents(["playback-track-changed"], async event => {
    if (event.type === TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED) {
      const track = await TrackPlayer.getTrack(event.nextTrack);
      if(currentPlay !== event.nextTrack){
        autoPlay(event.nextTrack);
      }
    }
  });

  const { style, currentPlay, autoPlay } = props;

  let togglePlayback;
  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    togglePlayback = (
      <IconButton
          icon={'pause-circle'}
          color={'white'}
          size={40}
          onPress={() => TrackPlayer.pause()}
      />
    );
  }
  else{
    togglePlayback = (
      <IconButton
          icon={'play-circle'}
          color={'white'}
          size={40}
          onPress={() => TrackPlayer.play()}
      />
    );
  }
  return (
    <View style={[styles.card, style]}>
      <View style={styles.controls}>
        <IconButton
            icon="step-backward"
            color={'white'}
            size={40}
            onPress={() => TrackPlayer.skipToPrevious()}
        />
        {togglePlayback}
        <IconButton
            icon="stop"
            color={'white'}
            size={40}
            onPress={() => {
              TrackPlayer.stop();
              autoPlay('');
            }}
        />
        <IconButton
            icon="step-forward"
            color={'white'}
            size={40}
            onPress={() => TrackPlayer.skipToNext()}
        />
      </View>
      <ProgressBar />
    </View>
  );
}

Player.propTypes = {
  style: ViewPropTypes.style,
  currentPlay: PropTypes.func.isRequired,
  autoPlay: PropTypes.func.isRequired
};

Player.defaultProps = {
  style: {}
};

const styles = StyleSheet.create({
  card: {
    width: "100%",
    alignItems: "center",
    backgroundColor:YELLOW_COLOR,
  },
  progress: {
    height: 3,
    width: "90%",
    marginBottom: 10,
    flexDirection: "row",
  },
  controls: {
    flexDirection: "row"
  },
});
